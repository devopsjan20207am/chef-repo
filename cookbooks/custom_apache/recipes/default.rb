#
# Cookbook:: custom_apache
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.


package 'httpd' do
  action :install
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

service "httpd" do
  action [ :start, :enable ]
end
