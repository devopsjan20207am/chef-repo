name 'custom_apache'
maintainer 'Anudeep'
maintainer_email 'anudeep@itopstube.com'
license 'All Rights Reserved'
description 'Installs/Configures apache webserver'
long_description 'Installs/Configures custom_apache'
version '0.1.1'
chef_version '>= 14.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/custom_apache/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/custom_apache'
