default['tomcat']['user'] = 'tomcat'
default['tomcat']['homedir'] = '/opt/tomcat'
default['tomcat']['group'] = 'tomcat'
default['tomcat']['downloadurl'] = 'http://apachemirror.wuchna.com/tomcat/tomcat-8/v8.5.51/bin/apache-tomcat-8.5.51.tar.gz'
default['tomcat']['homeprefix'] = '/opt'
default['tomcat']['version'] = 'apache-tomcat-8.5.51'
default['tomcat']['rootuser'] = 'root'
default['tomcat']['rootgroup'] = 'root'
