#
# Cookbook:: custom_tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

tomcat_password_keys = Chef::EncryptedDataBagItem.load("mysecrets","tomcatdata")
tomcat_admin_user = tomcat_password_keys["tomcatappuser"]
tomcat_admin_password = tomcat_password_keys["tomcatapppassword"]


package 'java-1.8.0-openjdk.x86_64' do
 action :install
end

user node['tomcat']['user'] do
  comment 'tomcat User'
  home node['tomcat']['homedir'] 
  system true
  shell '/bin/nologin'
end

group node['tomcat']['group'] do
  action :create
end

package 'wget' do
 action :install
end

ark 'tomcat' do
  url node['tomcat']['downloadurl'] 
  home_dir node['tomcat']['homedir'] 
  prefix_root node['tomcat']['homeprefix']
  owner node['tomcat']['user']
  version node['tomcat']['version'] 
end

execute 'GivingPermisions' do
 command 'chmod 755 /opt/tomcat/bin/*.sh'
end

template "/etc/systemd/system/tomcat.service" do
  source 'tomcat-init.erb'
  owner node['tomcat']['rootuser']
  group node['tomcat']['rootgroup']
  mode '0755'
  notifies :restart, 'service[tomcat]', :delayed
end

template "#{node['tomcat']['homedir']}/conf/tomcat-users.xml" do
  source 'tomcat-users.xml.erb'
  variables(:mytomcatadminuser => tomcat_admin_user, :mytomcatadminpassword => tomcat_admin_password)
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "#{node['tomcat']['homedir']}/webapps/manager/META-INF/context.xml" do
  source 'manager-context.xml.erb'
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

service 'tomcat' do
  supports :restart => true, :start => true, :stop => true 
  action [ :enable, :start ]
end





